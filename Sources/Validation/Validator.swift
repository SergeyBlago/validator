import Foundation

class Validator {

    func validate(username: String) -> Bool {

        let rangeLength = 3...32
        let stringRegEx = "^(?![-.0-9])[A-Za-z0-9.-]+$"
        let emailRegEx = "^(?![-.0-9])[a-zA-Z0-9.-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z0-9.-]+$"

        guard rangeLength.contains(username.count) else {
            return (false)
        }

        let results = [checkRegEx(in: username, regex: stringRegEx),
                       checkRegEx(in: username, regex: emailRegEx)]

        return results.filter {$0}.first ?? false

    }

    private func checkRegEx(in str: String, regex: String) -> Bool {

        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: str, range: NSRange(location: 0, length: str.count))

            if results.count == 0 {
                return false
            }

        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            return false
        }

        return true
    }
    
    //error

}
