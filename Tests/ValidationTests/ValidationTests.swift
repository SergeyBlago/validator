import XCTest
import Foundation
@testable import Validation

final class ValidationTests: XCTestCase {

  func testValidatorLength() {
    let tag = "ValidatorLength Error"
    var string = "ab"
    var result = Validator().validate(username: string)
    XCTAssert(!result, "\(tag): \(string))")

    string = "abc4567890abc4567890abc45678903333"
    result = Validator().validate(username: string)
    XCTAssert(!result, "\(tag): \(string))")
    
    string = "MikeStrong44Hand"
    result = Validator().validate(username: string)
    XCTAssert(result)

  }

  func testValidatorAlphabet() {
    let tag = "ValidatorAlphabet Error"
    let string = "Abcde..dsd.32343D-fds.fkj"
    let result = Validator().validate(username: string)

    XCTAssert(result, "\(tag): \(string))")


  }

  func testValidatorPrefix() {
    let tag = "ValidatorPrefix Error"
    let strings = [".qwerty", "1qwerty", "-qwerty"]

    for string in strings {

      let result = Validator().validate(username: string)
      if result {
        XCTAssert(false, "\(tag): \(string))")
      }
    }

    XCTAssert(true)


  }

  func testValidatorUseEmail() {
    let tag = "ValidatorUseEmail"
    let string = "Stri.n2-3g@drop.ru"
    let result = Validator().validate(username: string)
    XCTAssert(result, "\(tag): \(string))")

  }



}
